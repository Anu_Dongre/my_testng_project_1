package TestClass_TestngFileParameterization;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

//import API_Common_Methods.Common_Method_Handle_API;
//import API_Common_Methods.PATCH_Common_Method;
import API_Common_Methods.POST_Common_Method;
import Endpoint.POST_Endpoint;
import Request_Repository.POST_Request_Repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.restassured.path.json.JsonPath;

public class POST_TestCase_1 extends POST_Common_Method {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void test_setup() throws IOException {
		log_dir = Handle_directory.create_log_directory("POST_TestCase_1_logs");
		requestBody = POST_Request_Repository.post_request_tc1();
		endpoint = POST_Endpoint.POST_Endpoint_Tc1();
	}

	@Parameters({ "Req_name", "Req_job" })
	@Test()
	public static void Post_Executor(String Req_name, String Req_job) throws IOException {

		requestBody = "{\r\n" + "    \"name\": \"" + Req_name + "\",\r\n" + "    \"job\": \"" + Req_job + "\"\r\n"
				+ "}";

		for (int i = 0; i < 5; i++) {
			int statusCode = post_statusCode(requestBody, endpoint);
			System.out.println("POST API Triggered");
			System.out.println(statusCode);

			if (statusCode == 201) {
				String responseBody = post_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				POST_TestCase_1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("Expected statuscode of POST API (201) is not found , hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = POST_TestCase_1.class.getName();
		Handle_api_logs.evidence_creator(log_dir, testclassname, endpoint, requestBody, responseBody);
	}
}
