package Test_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.PUT_Common_Method;
import Endpoint.PUT_Endpoint;
import Request_Repository.PUT_Request_Repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.restassured.path.json.JsonPath;
public class PUT_TestCase_1 extends PUT_Common_Method {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void test_setup() throws IOException {
		log_dir = Handle_directory.create_log_directory("PUT_TestCase_1_logs");
		requestBody = PUT_Request_Repository.PUT_Request_Repository_Tc1();
		endpoint = PUT_Endpoint.PUT_Endpoint_Tc1();
	}
	@Test (description="Validate Response Body")
	public static void Put_Executor() throws IOException {
		for (int i = 0; i < 5; i++) {
			int statusCode = put_statusCode(requestBody, endpoint);
			System.out.println("PUT API Triggered");
			System.out.println(statusCode);

			if (statusCode == 200) {
				String responseBody = PUT_responseBody(requestBody, endpoint);
				System.out.println(responseBody);

				PUT_TestCase_1.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("Expected statuscode of PUT API (200) is not found , hence retrying");
			}
		}

	}
	private static String PUT_responseBody(String requestBody, String endpoint) {
		// TODO Auto-generated method stub
		return requestBody;
	}
	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		String req_createdAt = jsp_req.getString("createdAt");
		LocalDateTime currentdate = LocalDateTime.now();

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, req_createdAt);
	}
	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = PUT_TestCase_1.class.getName();
		Handle_api_logs.evidence_creator(log_dir, testclassname, endpoint, requestBody, responseBody);
	}
}
