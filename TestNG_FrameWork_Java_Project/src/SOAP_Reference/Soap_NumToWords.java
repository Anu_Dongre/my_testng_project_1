package SOAP_Reference;
import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
public class Soap_NumToWords{
	public static void main(String[] args) {
//step 1: Declare the Base URL
		String BaseURI = "https://www.dataaccess.com";
//step 2: Declare the request body
		String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n" + "   <soapenv:Body>\r\n" + "      <web:NumberToWords>\r\n"
				+ "         <web:ubiNum>14</web:ubiNum>\r\n" + "      </web:NumberToWords>\r\n"
				+ "   </soapenv:Body>\r\n" + "</soapenv:Envelope>";
//step3 Trigger the api and fetch the responsebody
		//RestAssured.baseURI = BaseURI;
		String ResponseBody = given().header("Content-Type", "text/xml;charset=utf-8").body(requestBody).when()
				.post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso").then().extract().response()
				.getBody().asString();
//Step4 print the response body
		System.out.println(ResponseBody);
//step5 extract the response body parameter
		XmlPath xml_res = new XmlPath(ResponseBody);
		String res_tag = xml_res.getString("NumberToWordsResult");
		System.out.println(res_tag);
//step6 Validate the responsebody
		Assert.assertEquals(res_tag, "fourteen ");

	}

}


