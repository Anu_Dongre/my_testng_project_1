package Utility_common_methods;
import java.io.File;

public class Handle_directory {
	
      public static File create_log_directory(String log_dir) {
		 //Fetch the current project directory
		 String project_dir = System.getProperty("user.dir");
		 System.out.println("The curent project directory path is : " +project_dir);
		 File directory = new File (project_dir +"\\API_logs\\" + log_dir);
		 
		 if(directory.exists())
		 {
			 directory.delete ();
			 System.out.println( directory + ": Deleted");

			 directory.mkdir();
			 System.out.println( directory + ": Created");
		 }
		 else 
		 {
			 directory.mkdir();
			 System.out.println( directory + ": Created");
			 
		 }
		 return directory;
	 }
	 
	}
