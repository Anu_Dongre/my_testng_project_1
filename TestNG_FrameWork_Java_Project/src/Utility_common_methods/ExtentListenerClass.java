package Utility_common_methods;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener {

	ExtentSparkReporter sparkReporter;
	ExtentReports extentReport;
	ExtentTest test;

	public void reportConfigurations() {
		sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html");
		extentReport = new ExtentReports();

		extentReport.attachReporter(sparkReporter);

//		adding system/environment information to reports
		extentReport.setSystemInfo("OS", "Windows 11");
		extentReport.setSystemInfo("Users", "Anuradha");

//		Configurations for changing look & feel of report
		sparkReporter.config().setDocumentTitle("RestAssured Extent Listener Report");
		sparkReporter.config().setReportName("This is my First Extent-Report");
		sparkReporter.config().setTheme(Theme.DARK);

	}

//	This method will get invoked before start of test case execution. (Same as @BeforeClass Annotation i.e. invokes only once}
	public void onStart(ITestContext result) {
		reportConfigurations();
		System.out.println("On Start Method Invoked...");
	}

//	This method will get invoked before start of test case execution.
	public void onFinish(ITestContext result) {
		System.out.println("On Finished Method Invoked...");
		extentReport.flush();
	}

//	This method will get invoked whenever any of the test case fails.
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of the test method failed: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.FAIL,
				MarkupHelper.createLabel("Name of the failed test case is: " + result.getName(), ExtentColor.RED));
	}

//	This method will get invoked whenever any of the test case is skipped.
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of the test method skipped: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.SKIP,
				MarkupHelper.createLabel("Name of the skipped test case is: " + result.getName(), ExtentColor.YELLOW));
	}

//	This method will get invoked on execution of each test case. (Same as @BeforeClass Annotation i.e. 4 times in our project)
	public void onTestStart(ITestResult result) {
		System.out.println("Name of the test method started: " + result.getName());
	}

//	This method will get invoked whenever any of the test case pass.
	public void onTestSuccess(ITestResult result) {
		System.out.println("Name of the test method executed successfully: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("Name of the passed test case is: " + result.getName(), ExtentColor.GREEN));
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
//		no implementation of this method, as it is not being used in project.
	}

}
