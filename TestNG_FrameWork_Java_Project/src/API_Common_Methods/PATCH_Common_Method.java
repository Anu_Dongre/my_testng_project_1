package API_Common_Methods;
import static io.restassured.RestAssured.given;

public class PATCH_Common_Method {


     public static int patch_statusCode(String requestBody, String endpoint) 
     { 
    	int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
        .then().extract().statusCode(); return statusCode;
     }
  
    public static String patch_responseBody(String requestBody, String endpoint)
    {
      String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
      .then().extract().response().asString(); return responseBody;
    
    }
}
  