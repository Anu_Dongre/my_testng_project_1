package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class PUT_Request_Repository 
{
 public static String PUT_Request_Repository_Tc1() throws IOException
 {
	 
	   ArrayList<String> Data = Excel_data_extractor.Excel_data_reader("TestData","Put_API","Put_TC2");
	   
	   System.out.println(Data);
	   String name = Data.get(1);
	   String job = Data.get(2);
	   
	   
		String requestBody = "{\r\n" + "   " + " \"name\": \""+name+" \",\r\n" + "  "
				+ "  \"job\": \""+job+"\"\r\n" + "}";
		return requestBody; 

 }
}
