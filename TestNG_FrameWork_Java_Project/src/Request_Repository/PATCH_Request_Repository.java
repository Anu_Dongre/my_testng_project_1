package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class PATCH_Request_Repository
{
   public static String PATCH_Request_Repository_Tc1() throws IOException 
   {
	   ArrayList<String> Data = Excel_data_extractor.Excel_data_reader("TestData", "Patch_API", "Patch_TC2");
	   
	   System.out.println(Data);
	   String name = Data.get(1);
	   String job = Data.get(2);
	   
	   String requestBody = "{\r\n" + "   " + " \"name\": \""+name+"\",\r\n" + "  "
				+ "  \"job\": \""+job+"\"\r\n" + "}";
	   return requestBody;

   }
}
